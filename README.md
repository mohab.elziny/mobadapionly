## MobAd API

## Getting Started

The following instructions will allow you to integrate the MobAd API in your android app using android studio.

### Usage:

**Step 1**. Initialize the user using the following end point `appInstanceService/app/register/initUser`

* Add the following as a request body:

```
{
  "idfa": "string",
  "idfv": "string",
  "appId": "string",
  "countryCode": "string"
}
```

* Add the Bearer Token `APP_ACCESS_TOKEN` to the `Authorization` header.

The response will be like the following:

```
{
    "data": {
        "id": "string",
        "adCap": 0,
        "hasSetLanguages": true,
        "hasDefaultInterests": true,
        "idfv": "string",
        "sdkId": "string",
        "pushId": "string",
        "lastRequestAt": "2023-01-09T08:21:45.428Z",
        "isOptedOut": true,
        "lastGetAdRequestTrigger": "string",
        "integratingAppId": "string",
        "countryId": "string",
        "token": "string",
        "tokenLifeTimeInMillis": 0L,
        "interestIds": [
          "string"
        ],
        "languageIdList": [
          "string"
        ]
    },
    "status": 0,
    "message": "string"
}
```

**Note**: we will provide you with the `APP_ACCESS_TOKEN` and `appId`

Example to get the `idfa`:

Add the following dependency:
```
implementation 'com.google.android.gms:play-services-ads-identifier:18.0.1'
```

in Kotlin:
```
val adInfo = AdvertisingIdClient.getAdvertisingIdInfo(context)
val idfa = adInfo.id
```

or in Java
```
AdvertisingIdClient.Info adInfo = AdvertisingIdClient.getAdvertisingIdInfo(context);
String idfa = adInfo.getId();
```

Example to get the `idfv`(Android Id):

in Kotlin:

```
val idfv = Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
```

or in Java:

```
String idfv = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
```
**Notes**:
* You will need to save the `id`, `token`, and `tokenLifeTimeInMillis` locally which you will need later.
* No need to initialize the user again if you have saved the `id`, `token`, and `tokenLifeTimeInMillis` locally.

---

**Step 2**. Get an ad using the following end point `adService/ad/getAd`

* Add the Bearer Token `USER_ACCESS_TOKEN` to the `Authorization` header.
* Add the `USER_ID` to `appInstanceId` as a query.
* Add user's country code to `fallbackCountryCode` as a query to get the targeted ad.

The response will be like the following:
```
{
   "data": {
      "id": "string",
      "channel": "string",
      "contentType": "string",
      "showAdvertiserName": true,
      "advertiser": {
         "id": "string",
         "imagePath": "string",
         "name": "string"
      },
      "showBubbleOnAndroid": true,
      "actionTokens": [
         "string"
      ],
      "content": [
         {
            "id": "string",
            "content": {
               "title": "string",
               "description": "string",
               "buttonTitle": "string",
               "url": "string"
            },
            "thumbnail": "string",
            "mediaPath": "string"
         }
      ]
   },
   "status": 0,
   "message": "string"
}
```
**Notes**:
* `USER_ACCESS_TOKEN` and `USER_ID` are values that you saved locally from initializing the user.
* You have to design different types of ads. 

---

**Step 3**. Get all actions that can be done by the user using the following end point `statisticService/action/fetchActions`

* Add the Bearer Token `USER_ACCESS_TOKEN` to `Authorization` in the header.

The response will be like the following:
```
{
   "data":{
      "actions": [
         "string"
      ]
   },
   "status": 0,
   "message": "string"
}
```

---

**Step 4**. Sending user's actions on the ad using the following end point `statisticService/action/register`

* Add the Bearer Token `USER_ACCESS_TOKEN` to `Authorization` in the header.
* Add the following as a request body:
```
{
  "actionKey": "string",
  "campaignId": "string",
  "tokenKey": "string"
}
```
* `actionKey`: is one of the actions list you fetched from the server.
* `campaignId`: is the ad id.
* `tokenKey`: is one token from the token list you received inside the get ad response.

**Action keys** Scenarios:
* Coordinating with us to discuss scenarios for ad actions.

The response will be like the follwoing
```
{
   "data":{
      "data": "string",
      "httpStatus": 0
   },
   "status": 0,
   "message": "string"
}
```

**Note**: You have to send each user action that we agreed together to the server.

---

##### Refresh user access token

You have to check the `tokenLifeTimeInMillis` if expired you will need to refresh the `USER_ACCESS_TOKEN` using `appInstanceService/profile/refreshUserToken` end point.

* Add the old Bearer Token `USER_ACCESS_TOKEN` to the `Authorization` header.

The response will be like the following:
```
{
   "data":{
      "token": "string",
      "tokenLifeTimeInMillis": 0
   },
   "status": 0,
   "message": "string"
}
```

---


For further information contact us.